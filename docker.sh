#!/bin/bash

set -e

curl -L --user $DATOMIC_USER:$DATOMIC_PASSWORD https://my.datomic.com/repo/com/datomic/datomic-pro/$DATOMIC_VERSION/datomic-pro-$DATOMIC_VERSION.zip -o /datomic-pro-$DATOMIC_VERSION.zip 
unzip /datomic-pro-$DATOMIC_VERSION.zip -d /opt
ln -s /opt/datomic-pro-$DATOMIC_VERSION /opt/datomic-pro

mkdir -p /etc/datomic

cat <<EOF > /etc/datomic/datomic-transactor.properties
protocol=dev
host=127.0.0.1
port=4334

license-key=$DATOMIC_LICENSE_KEY

memory-index-threshold=32m
memory-index-max=256m
object-cache-max=128m
EOF

cat <<EOF > /opt/datomic-pro/bin/logback.xml
<configuration>
   <contextListener class="ch.qos.logback.classic.jul.LevelChangePropagator">
    <resetJUL>true</resetJUL>
  </contextListener>
  <appender name="MAIN" class="ch.qos.logback.core.ConsoleAppender">
    <encoder>
      <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} %-5level %-10contextName %logger{36} - %msg%n</pattern>
    </encoder>
  </appender>
  <logger name="datomic.cast2slf4j" level="DEBUG"/>
  <logger name="org.apache.activemq.audit" level="WARN"/>
  <logger name="httpclient" level="INFO"/>
  <logger name="org.apache.commons.httpclient" level="INFO"/>
  <logger name="org.apache.http" level="INFO"/>
  <logger name="org.jets3t" level="INFO"/>
  <logger name="com.amazonaws" level="INFO"/>
  <logger name="com.amazonaws.request" level="WARN"/>
  <logger name="sun.rmi" level="INFO"/>
  <logger name="net.spy.memcached" level="INFO"/>
  <logger name="com.couchbase.client" level="INFO"/>
  <logger name="com.ning.http.client.providers.netty" level="INFO"/>
  <logger name="org.eclipse.jetty" level="INFO"/>
  <logger name="org.hornetq.core.client.impl" level="INFO"/>
  <logger name="org.apache.tomcat.jdbc.pool" level="INFO"/>
  <logger name="datomic.cast2slf4j" level="DEBUG"/>
  <root level="info">
    <appender-ref ref="MAIN"/>
  </root>
</configuration>
EOF

/opt/datomic-pro/bin/transactor /etc/datomic/datomic-transactor.properties &

test_port() {
    echo "(try (with-open [s (java.net.Socket. \"localhost\" $1)] (println \"ok\")) (catch Exception _ (println \"fail\")))" | /opt/datomic-pro/bin/run -
}

while [ "$(test_port 4334)" = "fail" ]; do
    echo 'Transactor not running yet, waiting...'
    sleep 5
done

echo "(require 'datomic.api) (datomic.api/create-database \"datomic:dev://localhost:4334/${DATOMIC_DB_NAME:-datomic}\")" | /opt/datomic-pro/bin/run -

/opt/datomic-pro/bin/run -m datomic.peer-server -h 127.0.0.1 -p ${DATOMIC_PEER_PORT:-8001} -a ${DATOMIC_PEER_ACCESS_KEY:-datomic},${DATOMIC_PEER_SECRET:-datomic} -d ${DATOMIC_DB_NAME:-datomic},datomic:dev://localhost:4334/${DATOMIC_DB_NAME:-datomic}