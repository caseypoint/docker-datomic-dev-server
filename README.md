# Datomic Dev Server

This image launches a Datomic peer-server instance, with a single, empty database initialized.

The image won't contain the actual Datomic distribution, the Datomic distribution will be downloaded when this image is run; you will need a Datomic account and a license key that is valid for the version you are using.

Influential environment variables:

* `DATOMIC_USER`: your user name (email address) on my.datomic.com. Required.
* `DATOMIC_PASSWORD`: your password on my.datomic.com. Required.
* `DATOMIC_LICENSE_KEY`: your license key. **Must** be valid for the version of Datomic you are trying to run (Datomic starter edition will work). Required.
* `DATOMIC_VERSION`: the version of Datomic to start. Optional, defaults to `1.0.6269`.
* `DATOMIC_DB_NAME`: the name of the database to create. Optional, defaults to `datomic`.
* `DATOMIC_PEER_ACCESS_KEY`: the peer server access key. Optional, defaults to `datomic`.
* `DATOMIC_PEER_SECRET`: the peer server secret. Optional, defaults to `datomic`.
* `DATOMIC_PEER_PORT`: the port the peer should listen on. Optional, defaults to `8001`.

Example running:

```
$ cat <<EOF > config.env
DATOMIC_USER=xxx
DATOMIC_PASSWORD=xxx
DATOMIC_LICENSE_KEY=xxx
EOF
$ docker run --env-file config.env -p 8001:8001 rsdio/datomic-dev-server
```
