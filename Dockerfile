FROM openjdk:11.0.8
ENV DATOMIC_USER=none
ENV DATOMIC_PASSWORD=none
ENV DATOMIC_VERSION=1.0.6269
COPY docker.sh /
ENTRYPOINT [ "/bin/bash", "/docker.sh" ]